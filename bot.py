""" This module provides a Bot class that allows a creation of a basic IRC bot. The class contains two methods that allow connecting to an IRC server and sending RAW messages to the server. """
import socket

class Bot(object):
    """ This class provides connect(), sendRaw(raw) and getAuth(nick) functions needed to connect to IRC, send RAW IRC commands and get user auth information
        It's parameters are server, port, channels, botnick, botdesc, user(pass empty string to use botnick), trigger, kickreason """
    def __init__(self, server="localhost", port=6667, channels=["#channel"], botnick="nick", botdesc="I'm a bot", user="", trigger="!", kickreason="Kicked"):
        self.server = server
        self.port = port
        self.channels = channels
        self.botnick = botnick
        self.botdesc = botdesc
        self.user = user if user else botnick
        self.trigger = trigger
        self.kickreason = kickreason
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.makefile = ""
        self.modules = {}
        self._adminlist = []
        self._modlist = []

    @property
    def adminlist(self):
        return self._adminlist

    @adminlist.setter
    def adminlist(self, val):
        self._adminlist.append(val)
        self._modlist.append(val)

    @property
    def modlist(self):
        return self._modlist

    @modlist.setter
    def modlist(self, val):
        self._modlist.append(val)

    def connect(self):
        """ Function used for connecting to IRC """
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.irc.settimeout(30)
        while 1:
            try:
                self.irc.connect((self.server, self.port))
            except socket.gaierror:
                continue
            break
        self.irc.settimeout(420)
        self.irc.send(bytes("USER " + self.botnick + " " + self.botnick + " " + self.botnick + " :" + self.botdesc + "\n", 'UTF-8'))
        self.irc.send(bytes("NICK " + self.botnick + "\n", 'UTF-8'))
        self.makefile = self.irc.makefile()

    def getAuth(self, nick):
        """ Function used for getting auth data of a user """
        auth = "none"
        self.irc.send(bytes('WHOIS ' + nick + '\r\n', 'UTF-8'))
        while 1:
            line = self.makefile.readline()
            if line.find(' 330 ') != -1:
                auth = line.split()[4]
                break
            elif line.find(' 318 ') != -1:
                break
        return auth

    def sendRaw(self, raw):
        """ Function used to send a raw IRC message """
        self.irc.send(bytes(raw + '\r\n', 'UTF-8'))

    def sendMessage(self, target, message):
        """ Function used to send messages to a channel or a person """
        self.irc.send(bytes('PRIVMSG ' + target + ' :' + message + '\r\n', 'UTF-8'))

    def addModules(self, modules):
        for key, value in modules.items():
            self.modules[':' + str(self.trigger) + str(key)] = value

