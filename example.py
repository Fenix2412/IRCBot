#!/usr/bin/python3
# Original Author: wyldbrian (https://github.com/wyldbrian)

# Import the socket module required for detecting timeouts and the bot class itself

import socket
import bot as IRCBot

# Declaration a new instance of the bot class
# It accepts: connection adress, connection port, a list of channels to join automatically after connecting,
# nickname for the bot, it's description shown to other IRC users, IRC user used for authing,
# the trigger symbol used in IRC commands like join (e.g. $join) and the default kick reason

bot = IRCBot.Bot("irc.quakenet.org", 6667, ["#channel"], "Bot", "I'm a bot", "botuser", "$", "See ya later mate")

# Import bot specific modules that provide new functionality

import modules.basicModules as basicModules
import modules.quoteModule as quoteModule
import modules.databaseModule as database
import modules.authQModule as authQModule

# Fetching users with admin privileges from the database connected via databaseModule and load the IRC interaction modules into the bot modules list

database.fetchAdmins(bot)
bot.addModules(basicModules.newModules)
bot.addModules(quoteModule.newModules)

# Connecting to the IRC

bot.connect()

# Main loop that handles keeping the connection up and receiving new messages

while 1:
    # Getting new messages and handling lost connection
    try:
        text = bot.makefile.readline()
    except socket.timeout:
        while 1:
            try:
                print('LOST CONNECTION')
                bot.connect()
            except socket.timeout:
                continue
            break
    if text.find('PING') != -1:
        bot.sendRaw('PONG ' + text.split()[1] + '\r\n')
    # Authing the bot and joining the channels specified in the autojoin list after the IRC server has sent the MOTD
    # Note: This auth module only works on quakenet but any other suitable auth module should be used at the same time
    # unless specified otherwise by the creator
    elif text.find('376 ' + bot.botnick) != -1 or text.find('422 ' + bot.botnick) != -1:
        authQModule.auth(bot, 'authpass')
        for chan in bot.channels:
            bot.sendRaw('JOIN ' + chan + '\r\n')
    else:
        # Debug message printing
        print(text)
        # Extracting the message type (e.g. :$join or :$kick)
        if len(text.split()) >= 4:
            msgType = text.split()[3]
        else:
            msgType = 'none'
        # Checking if there is a module that should take care of processing that trigger
        if msgType in bot.modules:
            bot.modules[msgType](bot, text)
